var wasmLoaded = false;

var Module = {
  wasmBinaryFile: "./example.wasm",
  onRuntimeInitialized: function() {
    wasmLoaded = true;
    console.log("Wasm loaded");
  }
};

async function wasmLoadWait() {
  // Early exit
  if (wasmLoaded === true) {
    return true
  }
  let check = function() {
    return new Promise( load => {
      setTimeout(() => {
        load(wasmLoaded);
      }, 10);
    });
  };
  // Require an async check because the function captures the global
  // as it is when the function is called - meaning infinite loops
  while (!await check()) {}
}

async function world() {
  // prevent body progress unless the Wasm module is loaded
  await wasmLoadWait();

  let out_ptr = Module._malloc();
  // create a pointer to a pointer, this will be passed to 
  // the Rust lib so that a new pointer can be written to it
  Module.setValue(out_ptr, 0, '*');

  if (Module._world_ptr(out_ptr) === 0) {
    return [true, out_ptr];
  }
  return [false, out_ptr];
}

async function print_world(ptr) {
  await wasmLoadWait();
  if (Module._print_world(ptr) === 0) {
    return true
  }
  false
} 

async function free_world(ptr) {
  await wasmLoadWait();
  if (Module._free_world(ptr) === 0) {
    return true
  }
  false
}