#[allow(dead_code)]
fn main() {}

#[derive(Debug)]
#[no_mangle]
pub struct World {
    w: String,
}

#[no_mangle]
pub unsafe extern "C" fn world_ptr(out_ptr: *mut usize) -> i8 {
	// You really don't want to try assigning something to the location
	// the pointer on the stack points to if it's null or invalid
	// We can't guarantee that the non-null address is fine though
	if out_ptr.is_null() {
        return -1;
    }
    // Must allocate on the heap or it will be lost to the void
    let world_in_a_box = Box::new(World {
        w: String::from("World"),
    });
    // turn it in to a raw pointer and cast to usize (pointer size)
    *out_ptr = Box::into_raw(world_in_a_box) as usize;
    0
}

/// Print the string located at the end of a double pointer to struct
#[no_mangle]
pub unsafe extern "C" fn print_world(ptr: *const usize) -> i8 {
    if ptr.is_null() {
        return -1;
    }
    // double deref the pointers to get the World struct
    // does not take ownership (you must still free the memory later)
    let world = &*(*ptr as *const World);
    println!("Hello, {}", world.w);
    0
}

/// Take ownership of the World struct again
#[no_mangle]
pub unsafe extern "C" fn free_world(ptr: *mut usize) -> i8 {
	if ptr.is_null() {
        return -1;
    }
    // dereference the ptr to get the usize, then cast that as a pointer to World
    let w: Box<World> = Box::from_raw(*ptr as *mut World);
    assert_eq!(w.w, "World");
    0
}

#[no_mangle]
pub extern "C" fn hello_world_ptr(
    _world_ptr: *const *const World,
    _out_ptr: *mut *const u8,
    _out_len: *mut u32,
) -> u8 {
    0
}

#[no_mangle]
pub extern "C" fn hello_world_buf(_in_ptr: *mut *mut u8, _in_len: u32) -> u8 {
    0
}

#[cfg(test)]
mod tests {
    use {world_ptr, print_world, free_world, World};
    #[test]
    fn test_world_ptr() {
    	let ptr_to_heap = Box::into_raw(Box::new(0usize));
        unsafe {
            assert_eq!(world_ptr(ptr_to_heap), 0);
        }
        unsafe {
            assert_eq!(print_world(ptr_to_heap), 0);
        }
        unsafe {
            assert_eq!(free_world(ptr_to_heap), 0);
        }
    }
}
